package ru.elegion.develop.digitalriver;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;


public class MainActivity extends AppCompatActivity {

    EditText txtNum;
    Button btnRun;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        txtNum = findViewById(R.id.txtNum);
        btnRun = findViewById(R.id.btnRun);
        btnRun.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int num;
                try {
                    num = Integer.parseInt(txtNum.getText().toString());
                }
                catch (Exception e) {
                    num =1;
                }

                num += sumNumbers(num);
                txtNum.setText(Integer.toString(num));
            }
        });
    }

    private int sumNumbers(int num) {
        if (num<0 ) return 0;
        if (num < 10) return num;
        int sum =0;
        while (num >0) {
            sum += num % 10;
            num /= 10;
        }
        return  sum;
    }
}

